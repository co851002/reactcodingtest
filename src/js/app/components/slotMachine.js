import React, { Component } from 'react';
import SlotWheel from './slotwheel';

class SlotMachine extends Component {
  /**
   *
   * @param {*} props
   * set up the three wheel colors
   */
  constructor(props) {
    super(props);
    this.state = {
      firstColor: "yellow",
      secondColor: "blue",
      thirdColor: "red",
      result: "Press play to see if you're the lucky winner",
      credits: 100,
      winner: ""
    }
    this.spinWheels = this.spinWheels.bind(this);
  }

  /**
   * Spin the wheels
   */
  spinWheels(){
    this.getNumbers(); 
  }

  /**
   * Get 3 random numbers to set the colors of the each wheel
   */
  getNumbers(){
    var firstNumber = 0;
    var secondNumber = 0;
    var thirdNumber = 0;
    var isFirtsNumber = false;
    var isSecondNumber = false;
    var isThirdNumber = false;

    // Loop 3 times to store a random number for each box
    for(var num = 0; num < 3; num++){
      if(!isFirtsNumber){
        firstNumber = Math.floor(Math.random() * 4);
        isFirtsNumber = true;
      }else if(!isSecondNumber){
        secondNumber = Math.floor(Math.random() * 4);
        isSecondNumber = true;
      }else if(!isThirdNumber){
        thirdNumber = Math.floor(Math.random() * 4);
        isThirdNumber = true;
      }
    }

    this.setColors(firstNumber, secondNumber, thirdNumber);
  }

  /**
   * Check if the colors are the same.
   * @param {*} firstNumber
   * @param {*} secondNumber
   * @param {*} thirdNumber
   */
  checkWinner(firstNumber, secondNumber, thirdNumber){
    if(firstNumber === secondNumber && secondNumber === thirdNumber){
      this.setState({
        result: "You won 5 credits !",
        credits: this.state.credits + 5,
      });
    }else{
      this.setState({
        result: "Better luck next time...",
        credits: this.state.credits -1
      });
    }
  }

   /**
    * Set the background color of each wheel
    * @param {*} firstNumber
    * @param {*} secondNumber
    * @param {*} thirdNumber
    */
  setColors(firstNumber, secondNumber, thirdNumber){
    const colors = ["yellow", "blue", "red", "green"];
    this.setState({
      firstColor: colors[firstNumber],
      secondColor: colors[secondNumber],
      thirdColor: colors[thirdNumber]
    });

    this.checkWinner(firstNumber, secondNumber, thirdNumber);
  }

  render() {
    return (

        <div className="slotMachine">

          <div className="slotWheels">
            <SlotWheel bgColor={this.state.firstColor}/>
            <SlotWheel bgColor={this.state.secondColor}/>
            <SlotWheel bgColor={this.state.thirdColor}/>
          </div>

          <div className="spin" onClick={this.spinWheels}>Spin</div>

          <div className="credits">Balance:{this.state.credits}</div>

          <div className="result">{this.state.result}</div>

        </div>
    )
  }
}

export default SlotMachine;
