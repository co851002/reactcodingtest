import React, {PropTypes} from 'react';
import IconImage from 'images/icon.png';

const SlotWheel = (props) => {
  return (
    <div className="slotWheel" style={{backgroundColor:props.bgColor}}>
      <img className="icon" src={IconImage}/>
    </div>
  );
};

SlotWheel.propTypes = {
    bgColor: PropTypes.string.isRequired
}

export default SlotWheel;
