import {slotMachine} from './slotMachine';

describe('spinResult method', () => {

    it('should return true', () => {
        var firstNumner = 1, secondNumber = 1, thirdNumber = 1;
        expect(spinResult(firstNumner, secondNumber, thirdNumber)).toBe(true);
    });

    it('should return false', () => {
        let firstNumner = 4, secondNumber = 7, thirdNumber = 9;
        expect(spinResult(firstNumner, secondNumber, thirdNumber)).toBe(false);
    });

});
