import React, { Component } from 'react';
import SlotMachine from './components/SlotMachine';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <SlotMachine />
      </div>
    )
  }
}

export default App
